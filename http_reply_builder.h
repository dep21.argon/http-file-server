#ifndef HTTP_REPLY_BUILDER_H
#define HTTP_REPLY_BUILDER_H

#include "http_codec.h"
#include "html_builder.h"
#include "http_packet.h"

class HttpReplyBuilder
{
public:
    std::string buildReply(std::string request);
private:
    HttpReplyPacket buildReplyNotImplemented();
    HttpReplyPacket buildReplyNotFound(enum HttpRequestPacket::Method method);
    HttpReplyPacket buildReplyFile(enum HttpRequestPacket::Method method, std::string pathToFile);
    HttpReplyPacket buildReplyDir(enum HttpRequestPacket::Method method, std::string pathToDir);
private:
    HttpCodec m_codec;
    HtmlBuilder m_htmlBuilder;
};

#endif //HTTP_REPLY_BUILDER_H
