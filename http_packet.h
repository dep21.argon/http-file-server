#ifndef HTTP_PACKET
#define HTTP_PACKET

#include <string>
#include <list>

class HttpReplyHeader
{
public:
    std::string key;
    std::string value;
};

class HttpReplyPacket
{
public:
    int status;
    std::list <HttpReplyHeader> headerList;
    std::string content;
};

class HttpRequestPacket
{
public:
    enum Method{GET, HEAD, POST} method;
    std::string uri;
};

#endif //HTTP_PACKET
