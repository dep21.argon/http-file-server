#include "http_server.h"
#include <cstdlib>
#include <iostream>

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << " listen_port" << std::endl;
        return 1;
    }
    HttpServer server;
    uint16_t port = atoi(argv[1]);
    server.startListen(port);
    return 0;
}

