#ifndef HTTP_CODEC
#define HTTP_CODEC

#include <string>
#include "http_packet.h"

class HttpCodec
{
public:
    std::string httpPacketToString(bool * errFlag, HttpReplyPacket packet);
    HttpRequestPacket stringToHttpPacket(bool * errFlag, std::string string);
private:
    std::string statusToString(bool * errFlag, int status);
    std::string headerListToString(bool * errFlag, std::list <HttpReplyHeader> headerList);
};

#endif //HTTP_CODEC
