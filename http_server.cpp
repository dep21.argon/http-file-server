#include "http_server.h"
#include <functional>
#include <iostream>
#include "http_connection_handler.h"

using namespace std::placeholders;

HttpServer::HttpServer()
    : m_ioService(),
      m_acceptor(m_ioService)
{
}

void HttpServer::startListen(uint16_t listenPort)
{
    m_endpoint = std::make_shared<ip::tcp::endpoint>(ip::tcp::v4(), listenPort);
    m_acceptor.open(m_endpoint->protocol());
    m_acceptor.bind(*m_endpoint);
    m_acceptor.listen();
    doAccept();
    m_ioService.run();
}

void HttpServer::doAccept()
{
    m_socket = std::make_shared<ip::tcp::socket>(m_ioService);
    m_acceptor.async_accept(*m_socket, std::bind(&HttpServer::onAccept, this, _1));
}

void HttpServer::onAccept(boost::system::error_code error)
{
    if(!error)
    {
        auto httpConnection = std::make_shared<HttpConnectionHandler>(m_socket);
        httpConnection->start();
    }
    doAccept();
}
