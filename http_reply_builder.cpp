#include "http_reply_builder.h"
#include "http_packet.h"
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <fstream>
#include <sstream>
#include <string>


using namespace boost::filesystem;

std::string HttpReplyBuilder::buildReply(std::string request)
{
    std::string reply;
    bool errFlag;
    HttpRequestPacket httpRequest = m_codec.stringToHttpPacket(&errFlag, request);
    HttpReplyPacket httpReply;
    if(errFlag)
    {
        httpReply = buildReplyNotImplemented();
    }
    else
    {
        if((httpRequest.method == HttpRequestPacket::Method::GET) || (httpRequest.method == HttpRequestPacket::Method::HEAD))
        {
            if(exists(current_path().string() + httpRequest.uri))
            {
                if(is_regular_file(current_path().string() + httpRequest.uri))
                {
                    httpReply = buildReplyFile(httpRequest.method, httpRequest.uri);
                }
                else if(is_directory(current_path().string() + httpRequest.uri))
                {
                    httpReply = buildReplyDir(httpRequest.method, httpRequest.uri);
                }
                else
                {
                    httpReply = buildReplyNotFound(httpRequest.method);
                }
            }
            else
            {
                httpReply = buildReplyNotFound(httpRequest.method);
            }
        }
    }
    return m_codec.httpPacketToString(&errFlag, httpReply);
}

HttpReplyPacket HttpReplyBuilder::buildReplyNotImplemented()
{
    HttpReplyPacket httpReply;
    httpReply.status = 501;
    httpReply.content = m_htmlBuilder.createHtml(httpReply.status);
    HttpReplyHeader contentType;
    contentType.key = "Content-Type";
    contentType.value = "text/html";
    httpReply.headerList.push_back(contentType);
    HttpReplyHeader contentLength;
    contentLength.key = "Content-Length";
    contentLength.value = std::to_string(httpReply.content.length());
    httpReply.headerList.push_back(contentLength);
    HttpReplyHeader connectionClose;
    connectionClose.key = "Connection";
    connectionClose.value = "close";
    httpReply.headerList.push_back(connectionClose);
    return httpReply;
}

HttpReplyPacket HttpReplyBuilder::buildReplyNotFound(HttpRequestPacket::Method method)
{
    HttpReplyPacket httpReply;
    httpReply.status = 404;
    httpReply.content = m_htmlBuilder.createHtml(httpReply.status);
    HttpReplyHeader contentType;
    contentType.key = "Content-Type";
    contentType.value = "text/html";
    httpReply.headerList.push_back(contentType);
    HttpReplyHeader contentLength;
    contentLength.key = "Content-Length";
    contentLength.value = std::to_string(httpReply.content.length());
    httpReply.headerList.push_back(contentLength);
    HttpReplyHeader connectionClose;
    connectionClose.key = "Connection";
    connectionClose.value = "close";
    httpReply.headerList.push_back(connectionClose);
    if(method == HttpRequestPacket::Method::HEAD)
    {
        httpReply.content = "";
    }
    return httpReply;
}

HttpReplyPacket HttpReplyBuilder::buildReplyFile(HttpRequestPacket::Method method, std::string pathToFile)
{
    HttpReplyPacket httpReply;
    httpReply.status = 200;
    HttpReplyHeader contentType;
    contentType.key = "Content-Type";
    contentType.value = "text/plain";
    httpReply.headerList.push_back(contentType);
    HttpReplyHeader contentLength;
    contentLength.key = "Content-Length";
    contentLength.value = std::to_string(file_size(current_path().string() + pathToFile));
    httpReply.headerList.push_back(contentLength);
    HttpReplyHeader connectionClose;
    connectionClose.key = "Connection";
    connectionClose.value = "close";
    httpReply.headerList.push_back(connectionClose);
    if(method == HttpRequestPacket::Method::GET)
    {
        std::ifstream fileStream(current_path().string() + pathToFile, std::ios::in | std::ios::binary);
        if(!fileStream)
        {
            return buildReplyNotFound(method);
        }
        std::ostringstream content;
        content << fileStream.rdbuf();
        fileStream.close();
        httpReply.content = content.str();
    }
    return httpReply;
}

HttpReplyPacket HttpReplyBuilder::buildReplyDir(HttpRequestPacket::Method method, std::string pathToDir)
{
    HttpReplyPacket httpReply;
    httpReply.status = 200;
    std::list <FileStruct> filesList;
    for(auto file: boost::make_iterator_range(directory_iterator(current_path().string() + pathToDir), {}))
    {
        FileStruct fileStruct;
        fileStruct.path = pathToDir + file.path().filename().string();
        if(is_regular_file(file))
        {
            fileStruct.type = FT_REGULAR;
        }
        else if(is_directory(file))
        {
            fileStruct.type = FT_DIR;
        }
        filesList.push_back(fileStruct);
    }
    httpReply.content = m_htmlBuilder.createHtml(pathToDir, filesList);
    HttpReplyHeader contentType;
    contentType.key = "Content-Type";
    contentType.value = "text/html";
    httpReply.headerList.push_back(contentType);
    HttpReplyHeader contentLength;
    contentLength.key = "Content-Length";
    contentLength.value = std::to_string(httpReply.content.length());
    httpReply.headerList.push_back(contentLength);
    HttpReplyHeader connectionClose;
    connectionClose.key = "Connection";
    connectionClose.value = "close";
    httpReply.headerList.push_back(connectionClose);
    if(method == HttpRequestPacket::Method::HEAD)
    {
        httpReply.content = "";
    }
    return httpReply;
}
