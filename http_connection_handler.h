#ifndef HTTP_CONNECTION_HANDLER_H
#define HTTP_CONNECTION_HANDLER_H

#include <boost/asio.hpp>
#include <vector>
#include <array>
#include <memory>
#include "http_reply_builder.h"

using namespace boost::asio;

class HttpConnectionHandler
    :public std::enable_shared_from_this <HttpConnectionHandler>
{
public:
    HttpConnectionHandler(std::shared_ptr <ip::tcp::socket> socket);
    void start();
    void doRead();
    void onRead(boost::system::error_code error, std::size_t bytesRead);
    void doWrite(std::vector <char> rawData);
    void onWrite(boost::system::error_code error, std::size_t bytesWrite);
private:
    std::shared_ptr <ip::tcp::socket> m_socket;
    std::array<char, 1024> m_intermediateRequestBuffer;
    std::vector<char> m_fullRequestBuffer;
    std::vector<char> m_outBuffer;
    int m_totalWriteBytes;
    HttpReplyBuilder m_replyBuilder;
};

#endif //HTTP_CONNECTION_HANDLER_H
