#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <memory>
#include <boost/asio.hpp>

using namespace boost::asio;

class HttpServer
{
public:
	HttpServer();
	void startListen(uint16_t listenPort);
private:
    void doAccept();
    void onAccept(boost::system::error_code error);
private:
    std::shared_ptr <ip::tcp::endpoint> m_endpoint;
    io_service m_ioService;
    ip::tcp::acceptor m_acceptor;
    std::shared_ptr <ip::tcp::socket> m_socket;
};

#endif //HTTP_SERVER_H
