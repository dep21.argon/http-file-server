#ifndef HTML_BUILDER_H
#define HTML_BUILDER_H

#include <string>
#include <list>

enum fileType{FT_REGULAR, FT_DIR};

class FileStruct
{
public:
    std::string path;
	fileType type;
};

class HtmlBuilder
{
public:
    HtmlBuilder();
    std::string createHtml(int status);
	std::string createHtml(std::string currentDir, std::list <FileStruct> fileList);
private:
	std::string createHtmlBody(std::string currentDir, std::list <FileStruct> fileList);
};

#endif //HTML_BUILDER_H
