#include "http_codec.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string.hpp>

std::string HttpCodec::httpPacketToString(bool * errFlag, HttpReplyPacket packet)
{
    std::string answer;
    *errFlag = false;
    answer += statusToString(errFlag, packet.status);
    if(*errFlag)
    {
        return answer;
    }
    answer += headerListToString(errFlag, packet.headerList);
    if(*errFlag)
    {
        return answer;
    }
    answer += "\r\n";
    answer += packet.content;
    return answer;
}

HttpRequestPacket HttpCodec::stringToHttpPacket(bool *errFlag, std::string rawString)
{
    HttpRequestPacket request;
    *errFlag = false;
    auto endOfRequest = rawString.find("\r\n");
    rawString.resize(endOfRequest);
    //std::string rawString = boost::to_upper_copy(rawString);
    std::vector <std::string> words;
    boost::algorithm::split(words, rawString, boost::algorithm::is_any_of(" "));
    if(words.size() < 3)
    {
        *errFlag = true;
        return request;
    }
    if(words.at(0) == "HEAD")
    {
        request.method = HttpRequestPacket::Method::HEAD;
    }
    else if(words.at(0) == "GET")
    {
        request.method = HttpRequestPacket::Method::GET;
    }
    else if(words.at(0) == "POST")
    {
        request.method = HttpRequestPacket::Method::POST;
    }
    request.uri = words.at(1);
    std::vector <std::string> protocolVersion;
    boost::algorithm::split(protocolVersion, words.at(2), boost::algorithm::is_any_of("/"));
    if(protocolVersion.size() != 2)
    {
        *errFlag = true;
        return request;
    }
    if(protocolVersion.at(0) != "HTTP")
    {
        *errFlag = true;
        return request;
    }
    if((protocolVersion.at(1) != "1.0")
        && (protocolVersion.at(1) != "1.1"))
    {
        *errFlag = true;
        return request;
    }
    return request;
}

std::string HttpCodec::statusToString(bool *errFlag, int status)
{
    *errFlag = false;
    std::string statusStr;
    switch(status)
    {
    case 200:
        statusStr = "HTTP/1.0 200 OK\r\n";
        break;
    case 404:
        statusStr = "HTTP/1.0 404 Not Found\r\n";
        break;
    case 501:
        statusStr = "HTTP/1.0 501 Not Implemented\r\n";
        break;
    default:
        *errFlag = true;
        break;
    }
    return statusStr;
}

std::string HttpCodec::headerListToString(bool *errFlag, std::list<HttpReplyHeader> headerList)
{
    *errFlag = false;
    std::string headers;
    for(HttpReplyHeader header: headerList)
    {
        headers += header.key + ": " + header.value + "\r\n";
    }
    return headers;
}


