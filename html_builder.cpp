#include "html_builder.h"
#include <sstream>

HtmlBuilder::HtmlBuilder()
{
}

std::string HtmlBuilder::createHtml(int status)
{
    std::stringstream htmlDoc;
    htmlDoc << "<!DOCTYPE html>\n"
        << "<html>\n"
        << "\t<head>\n"
        << "\t\t<title>" << status << "</title>\n"
        << "\t</head>\n"
        << "\t<body>\n"
        << "\t\t<p>" << status << "</p>\n"
        << "\t</body>\n"
        << "</html>";
    return htmlDoc.str();
}

std::string HtmlBuilder::createHtml(std::string currentDir, std::list <FileStruct> fileList)
{
    std::stringstream htmlDoc;
    htmlDoc << "<!DOCTYPE html>\n"
        << "<html>\n"
        << "\t<head>\n"
        << "\t\t<title>Content of " << currentDir << "</title>\n"
        << "\t</head>\n"
        << "\t<body>\n"
        << createHtmlBody(currentDir, fileList)
        << "\t</body>\n"
        << "</html>";
    return htmlDoc.str();
}

std::string HtmlBuilder::createHtmlBody(std::string currentDir, std::list <FileStruct> fileList)
{
    std::stringstream htmlBody;
    htmlBody << "<p><a href=\"../\">../</a></p>";
	for(auto f: fileList)
	{
		std::string name;
		switch(f.type)
		{
		case FT_REGULAR:
			name = f.path;
			break;
		case FT_DIR:
			name = f.path + "/";
			break;
		}
        std::string path = currentDir + "/" + name;
        htmlBody << "<p><a href=\"" << name << "\">" << name << "</a></p>";
	}
    return htmlBody.str();
}
