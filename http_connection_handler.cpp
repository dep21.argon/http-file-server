#include "http_connection_handler.h"
#include <iostream>
#include <functional>

using namespace std::placeholders;

HttpConnectionHandler::HttpConnectionHandler(std::shared_ptr <ip::tcp::socket> socket)
{
    m_socket = socket;
}

void HttpConnectionHandler::start()
{
    doRead();
}

void HttpConnectionHandler::doRead()
{
    m_socket->async_read_some(::buffer(m_intermediateRequestBuffer),
                              std::bind(&HttpConnectionHandler::onRead,
                                        shared_from_this(),
                                        _1,
                                        _2));
}

void HttpConnectionHandler::onRead(boost::system::error_code error, std::size_t bytesRead)
{
    if(!error)
    {
        m_fullRequestBuffer.insert(m_fullRequestBuffer.end(),
                                   m_intermediateRequestBuffer.begin(),
                                   m_intermediateRequestBuffer.begin() + bytesRead);
        std::string inputRequest(m_fullRequestBuffer.begin(), m_fullRequestBuffer.end());
        auto endPosition = inputRequest.find("\r\n\r\n");
        if(endPosition == std::string::npos)
        {
            doRead();
            return;
        }
        std::string outputReply = m_replyBuilder.buildReply(inputRequest);
        std::vector<char> sndBuf(outputReply.begin(), outputReply.end());
        doWrite(sndBuf);
    }
    else
    {
        std::cout << "ERROR: " << error.message() << std::endl;
    }
}

void HttpConnectionHandler::doWrite(std::vector <char> rawData)
{
    m_outBuffer = rawData;
    async_write(*m_socket, ::buffer(m_outBuffer), std::bind(&HttpConnectionHandler::onWrite,
                                                 shared_from_this(),
                                                 _1,
                                                 _2));
}

void HttpConnectionHandler::onWrite(boost::system::error_code error, std::size_t bytesWrite)
{
    if(error)
    {
        std::cout << "ERROR: " << error.message() << std::endl;
    }
}
